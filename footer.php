        <footer id="footer">
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
            <div id="memberships">
                <h3>Memberships</h3>
                <a href="http://www.accs.org.au/" title="Australasian College of Cosmetic Surgery"><img src="<?php echo get_template_directory_uri(); ?>/img/accs.png" alt="Australasian College of Cosmetic Surgery" /></a>
                <a href="http://www.aslms.org/" title="American Society for Laser Medicine &amp; Surgery"><img src="<?php echo get_template_directory_uri(); ?>/img/aslms.png" alt="American Society for Laser Medicine &amp; Surgery" /></a>
                <a href="http://www.phlebology.com.au/" title="Australian College of Phlebology"><img src="<?php echo get_template_directory_uri(); ?>/img/acp.png" alt="Australian College of Phlebology" /></a>
                <a href="#" title="What Clinic Awards"><img src="<?php echo get_template_directory_uri(); ?>/img/what-clinic-awards.png" alt="What Clinic Awards" /></a>

            </div>
        </footer>

        <?php wp_footer() ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-64904731-1','auto');ga('send','pageview');
        </script>
    </body>
</html>