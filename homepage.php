<?php
/*
Template Name: Laserway Home
*/
?>

<?php get_header(); ?>
        <article id="main-content">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>
        </article>

        <script>

            function hexToRgb(hex) {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                } : null;
            }
            var color = '<?php the_field('page_colour'); ?>';
            var canvas = document.getElementById('slideshow-shape').getContext('2d');
            var rgbColor = hexToRgb(color).r + ',' + hexToRgb(color).g + ',' + hexToRgb(color).b;
            canvas.fillStyle = 'rgba(' + rgbColor + ',1)';
            canvas.beginPath();
            canvas.moveTo(396, 0);
            canvas.lineTo(396, 384);
            canvas.lineTo(8, 384);
            canvas.lineTo(166, 0);
            canvas.closePath();
            canvas.fill();
            canvas.fillStyle = 'rgba(' + rgbColor + ',0.5)';
            canvas.beginPath();
            canvas.moveTo(0, 0);
            canvas.lineTo(166, 0);
            canvas.lineTo(105, 150);
            canvas.closePath();
            canvas.fill();
        </script>
<?php get_footer(); ?>