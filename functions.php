<?php

function lw_scripts()
{
    wp_register_script( 'main-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ) );
    wp_register_script( 'plugins-js', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ) );
    wp_enqueue_script( 'main-js' );
    wp_enqueue_script( 'plugins-js' );
}
add_action( 'wp_enqueue_scripts', 'lw_scripts' );

//Register site menu

function register_laserway_menu() {
  register_nav_menu('site-menu',__( 'Site Menu' ));
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_laserway_menu' );

//Enable page thumbnails

add_theme_support( 'post-thumbnails' );

?>