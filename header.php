<!doctype html>
<html class="no-js" lang="en-au">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php the_title(); ?> | Laserway on Davey</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

        <link href='//fonts.googleapis.com/css?family=Asap:400,700|Lato:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>
        <style>
            #menu a:hover, #footer a:hover, #main-content-inner a, #main-content-inner h3{
                color: <?php the_field('page_colour'); ?>;
            }
            .tagline a:hover{
                background-color: <?php the_field('page_colour'); ?>;
            }

            @media(max-width: 700px){
                #main-content-top{
                    background-color: <?php the_field('page_colour'); ?>;
                }
            }
        </style>
        <?php wp_head() ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <a href="#menu" class="off-left">Skip to menu</a>
        <a href="#main-content" class="off-left">Skip to content</a>
        <h1 class="off-left">Laserway on Davey</h1>
        <header id="header">
            <img src="<?php echo get_template_directory_uri(); ?>/img/shape-overlay.png" id="shape-overlay" style="background-color: <?php the_field('page_colour'); ?>" alt=""  />
            <div id="header-inner">
                <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/header-logo.png" id="header-logo" style="background-color: <?php the_field('page_colour'); ?>" alt="Laserway on Davey homepage" /></a>
                <nav id="menu">
                    <a href="#" id="menu-mobile-link"><img src="<?php echo get_template_directory_uri(); ?>/img/menu-burger.png" alt="" />Menu</a>
                    <?php wp_nav_menu( array( 'theme_location' => 'site-menu' ) ); ?>
                </nav>
            </div>
        </header>