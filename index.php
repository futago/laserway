<?php get_header(); ?>
        <article id="main-content">
            <div id="main-content-top">
                <div id="content-banner-left"></div>
                <div id="content-banner-image">
                    <?php
                        if ( has_post_thumbnail() ) {
                            echo(get_the_post_thumbnail( ));
                        }
                    ?>
                    <!--<canvas width="285" height="262" id="content-banner-image-canvas"></canvas>-->
                </div>
                <div id="content-banner-text">
                    <canvas width="379" height="262" id="content-banner-text-canvas"></canvas>
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
                <div id="content-banner-right" style="background-color: <?php the_field('page_colour'); ?>"></div>
            </div>
            <div id="main-content-inner">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </article>
        <script>
            function hexToRgb(hex) {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                } : null;
            }
            var color = '<?php the_field('page_colour'); ?>';
            var rgbColor = hexToRgb(color).r + ',' + hexToRgb(color).g + ',' + hexToRgb(color).b;
            /*
            var canvas = document.getElementById('content-banner-image-canvas').getContext('2d');
            canvas.fillStyle = 'rgba(170, 170, 170, 0.5)';
            canvas.beginPath();
            canvas.moveTo(0, 0);
            canvas.lineTo(285, 0);
            canvas.lineTo(72, 262);
            canvas.lineTo(0, 262);
            canvas.closePath();
            canvas.fill();
            */
            var canvas2 = document.getElementById('content-banner-text-canvas').getContext('2d');
            canvas2.fillStyle = 'rgba(' + rgbColor + ',1)';
            canvas2.beginPath();
            canvas2.moveTo(379, 0);
            canvas2.lineTo(379, 262);
            canvas2.lineTo(26, 262);
            canvas2.lineTo(150, 0);
            canvas2.closePath();
            canvas2.fill();
            canvas2.fillStyle = 'rgba(' + rgbColor + ',0.3)';
            canvas2.beginPath();
            canvas2.moveTo(0, 0);
            canvas2.lineTo(150, 0);
            canvas2.lineTo(105, 104);
            canvas2.closePath();
            canvas2.fill();
        </script>
<?php get_footer(); ?>