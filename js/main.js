$j = jQuery.noConflict();

$j(document).ready(function(){

	//Slideshow

	$images = $j('.slideshow img');
	var limit = $images.length;
	var current = 0;
	$textspace = $j('.slideshow-text p');
	var next = 1;
	var slideshowTime = 8000;

	$images.each(function(i){
		if (i != 0){
			$j(this).hide();
		}
	})

	$textspace.text($images.eq(0).attr('alt'));

	function slideshow(){
		$images.eq(current).fadeOut({duration: 1000, queue: false});
		if (current + 1 != limit){
			next = current + 1;
			$images.eq(current + 1).fadeIn({duration: 1000, queue: false});
			$textspace.fadeOut(500, function(){
				$textspace.text($images.eq(next).attr('alt')).fadeIn(500);
			});
			current ++;
		}
		else{
			$images.eq(0).fadeIn({duration: 1000, queue: false});
			$textspace.fadeOut(500, function(){
				$textspace.text($images.eq(0).attr('alt')).fadeIn(500);
			});
			current = 0;
		}
		var t = setTimeout(slideshow, slideshowTime);
	}

	var t = setTimeout(slideshow, slideshowTime);


	// Homepage box heights

	if ($j('.box-row').length > 0){
		function boxHeights(){
			var height = 0;
			$j('.box-row .panel-grid-cell .textwidget').each(function(){
				var boxHeight = $j(this).height();
				if (boxHeight > height){
					height = boxHeight;
				}
			});
			$j('.box-row .panel-grid-cell .textwidget').each(function(){
				$j(this).height(height);
			})
		};
		$j(window).resize(function(){
			boxHeights();
		})
		boxHeights();
	};


	// Mobile menu

	$j('#menu-mobile-link').click(function(e){
		e.preventDefault();
		$j('#menu ul').slideToggle();
	})

})